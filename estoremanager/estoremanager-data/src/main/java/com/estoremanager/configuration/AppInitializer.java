package com.estoremanager.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.estoremanager.utill.IConstant;


public class AppInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext servletContext) throws ServletException {

		AnnotationConfigWebApplicationContext annotationConfigWebApplicationContext = new AnnotationConfigWebApplicationContext();
		annotationConfigWebApplicationContext.register(AppConfig.class);
		servletContext.addListener(new ContextLoaderListener(annotationConfigWebApplicationContext));
		annotationConfigWebApplicationContext.setServletContext(servletContext);
		
		/*add New Code to stop this exception
		 * 
		 * java.lang.IllegalStateException: BeanFactory not initialized or already closed - call 'refresh' 
		 * -before accessing beans via the ApplicationContext
		 * 
		 * 
		 * */
		
		/*annotationConfigWebApplicationContext.refresh();*/
		
		Dynamic servlet = servletContext.addServlet(IConstant.DISPATCHER_SERVLET_NAME, new DispatcherServlet(annotationConfigWebApplicationContext));
		servlet.addMapping("/");
		servlet.setLoadOnStartup(IConstant.INT_ONE);
		servlet.setInitParameter("throwExceptionIfNoHandlerFound","true");

	}

}
