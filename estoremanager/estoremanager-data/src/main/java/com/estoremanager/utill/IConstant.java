package com.estoremanager.utill;

/**
 * Hold all Static elements used in project
 * 
 * @author Aartek
 *
 */
public class IConstant {

	public static final Byte IS_DELETED = 1;
	public static final String STATIC_AWS_URL = "https://s3-us-west-2.amazonaws.com/birddogattachments/";
	public static final String leadFolderName = "Lead_Images";
	public static final String birddogProfileFolderName = "Birddog_Profile_Images";
	public static final String superAdminProfileFolderName = "Super_Admin_Profile_Images";
	public static final String reiProfileFolderName = "Rei_Profile_Images";
	public static final String pdfUploadedByAdmin = "Pdf_Uploaded_By_Admin";
	public static final String softwareUploadedByAdmin = "Software_Uploaded_By_Admin";
	public static final String uploadW9Pdf = "Upload_W9_Pdf";

	// Mail Credentials
	public static final String FROM_EMAIL_ID = "birddog@birddoggenerator.com";
	public static final String FROM_PASSWORD = "123456789";
	public static final String SMTP_HOST = "smtpout.secureserver.net";
	public static final String SMTP_AUTH = "true";
	public static final String SMTP_PORT = "465";
	public static final String SMTP_SOCKET_FACTORY = "javax.net.ssl.SSLSocketFactory";

	public static final String PROP_SMTP_HOST = "mail.smtp.host";
	public static final String PROP_SMTP_AUTH = "mail.smtp.auth";
	public static final String PROP_SMTP_PORT = "mail.smtp.port";
	public static final String PROP_SMTP_SOCKET_FACTORY = "mail.smtp.socketFactory.class";

	// System
	public static final int MULTIPART_MAX_UPLOAD_SIZE = 104857600;/* 20971520 */
	public static final int MULTIPART_MAX_IN_MEMORY_SIZE = 104857600;

	// paths to files
	// public static final String BIRDDOG_IMAGES = "G:/intern/";
	// public static final String REINVESTOR_IMAGES = "G:/intern/";
	// public static final String BIRDDOG_PROFILE_IMAGES = "G:/intern/";
	// public static final String SUPER_ADMIN_IMAGES = "G:/intern/";
	// public static final String SUPER_ADMIN_VIDEO = "G:/intern/";
	//
	// public static final String PDF_AND_SW_PATH = "G:/intern/";

	// public static final String BIRDDOG_IMAGES =
	// "/home/grrajain/jvm/apache-tomcat-7.0.47/domains/rentmanager.princeglobalconsultants.com/birddogUploads/bird_images/";
	// public static final String REINVESTOR_IMAGES =
	// "/home/grrajain/jvm/apache-tomcat-7.0.47/domains/rentmanager.princeglobalconsultants.com/birddogUploads/rei_images/";
	// public static final String BIRDDOG_PROFILE_IMAGES =
	// "/home/grrajain/jvm/apache-tomcat-7.0.47/domains/rentmanager.princeglobalconsultants.com/birddogUploads/bird_profile_images/";
	// public static final String SUPER_ADMIN_IMAGES =
	// "/home/grrajain/jvm/apache-tomcat-7.0.47/domains/rentmanager.princeglobalconsultants.com/birddogUploads/super_admin_images/";
	// public static final String SUPER_ADMIN_VIDEO =
	// "/home/grrajain/jvm/apache-tomcat-7.0.47/domains/rentmanager.princeglobalconsultants.com/birddogUploads/super_admin_video/";
	// public static final String PDF_AND_SW_PATH =
	// "/home/grrajain/jvm/apache-tomcat-7.0.47/domains/rentmanager.princeglobalconsultants.com/birddogUploads/bird_pdf/";
	//

	public static final String BIRDDOG_IMAGES = "https://s3-us-west-2.amazonaws.com/birddogattachments/Lead_images/";
	public static final String REINVESTOR_IMAGES = "https://s3-us-west-2.amazonaws.com/birddogattachments/Rei_Profile_Images/";
	public static final String BIRDDOG_PROFILE_IMAGES = "https://s3-us-west-2.amazonaws.com/birddogattachments/Birddog_Profile_Images/";
	public static final String SUPER_ADMIN_IMAGES = "https://s3-us-west-2.amazonaws.com/birddogattachments/Super_Admin_Profile_Images/";
	public static final String SUPER_ADMIN_VIDEO = "https://s3-us-west-2.amazonaws.com/birddogattachments/Video/";
	public static final String PDF_AND_SW_PATH = "https://s3-us-west-2.amazonaws.com/birddogattachments/Pdf_Software/";

	// User Roles
	public static final String USER_ROLE_RE_INVESTOR = "REI";
	public static final String USER_ROLE_SUPER_ADMIN = "SA";
	public static final String USER_ROLE_BIRDDOG = "BIRDDOG";
	public static final String USER_ROLE_AMONYMOUSE = "anonymousUser";

	// User Role's Id
	public static final int USER_ROLE_RE_INVESTOR_ID = 1;
	public static final int USER_ROLE_SUPER_ADMIN_ID = 3;
	public static final int USER_ROLE_BIRDDOG_ID = 2;

	// User Role's Id for AWS
	public static final int REI_ROLE = 1;
	public static final int BIRDDOG_ROLE = 2;
	public static final int ADMIN_ROLE = 3;
	public static final int USER_ROLE = 4;

	public static final String RE_INVESTOR = "reInvestor";
	public static final String PRODUCT_VERSIONS = "productVersions";
	public static final String RE_DIRECT = "redirect:";

	public static final String IMAGE = "image";

	public static final String MULTIPART_RESOLVER = "multipartResolver";
	public static final String DISPATCHER_SERVLET_NAME = "dispatcher";
	public static final String JPA_REPOSITORY = "com.estoremanager.repository";
	public static final String HIBERNATE_PROPERTY_SOURCE = "classpath:hibernate.properties";
	public static final String HIBERNATE_COMPONENT_SCAN = "com.estoremanager.configuration";
	public static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";

	// key to display message on jsp
	public static final String MESSAGE = "message";
	public static final String REI_UNIQUE_ID = "reiId";

	public static final Byte INT_ONE = 1;
	public static final Integer ONE = 1;

	public static final String APPROVED = "Approved";
	public static final String STATUS = "Success";
	public static final String MESSAGES = "messages";
	public static final String MESAGE = "mesage";

	public static final String DRIVER_CLASS_NAME = "jdbc.driverClassName";
	public static final String DB_URL = "jdbc.url";
	public static final String DB_USER = "jdbc.username";
	public static final String DB_PASSWORD = "jdbc.password";

	public static final String HIBERNATE_DILECT = "hibernate.dialect";
	public static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
	public static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
	public static final String CACHE_PROVIDER_CLASS = "hibernate.cache.provider_class";
	public static final String CACHE_USE_SECOND_LEVEL_CACHE = "hibernate.cache.use_second_level_cache";
	public static final String HIBERNATE_CACHE_REGION_FACTORY_CLASS = "hibernate.cache.region.factory_class";
	public static final String USE_QUERY_CACHE = "hibernate.cache.use_query_cache";
	
	public static final String BASE_PACKAGE = "com.estoremanager";
	public static final String PROPERTY_SOURCE = "classpath:log4j.properties";

	public static final String USER_DETAIL_SERVICE = "userDetailsService";
	public static final String NOTIFICTION_LEAD_URL = "/notificationLead";
	public static final String PAYMENT_RECORD = "Payment";
	
	public static final String USER_NAME = "userName";
	public static final String PASSWORD = "password";
	public static final String EMAIL = "email";
	public static final String VIEW_PAYMENT_BIRDDOG_BY_REI  = "viewpaymentbirddog";

	public static final String HOME = "home";
	public static final String ERROR = "error";
	public static final String LOGOUT = "logout";
	public static final String BIRDDOG = "birddog";
	public static final String INVESTOR = "investor";
	public static final String SUPER_ADMIN = "superAdmin";
	public static final String INVESTOR_ID = "investorId";
	public static final String REI_ID = "reiId";
	public static final String ROLE_ID = "roleId";
	public static final String BIRDDOG_LIST = "birddogsList";
	public static final String VIEW_PAYMENT_LEAD = "viewPaymentLead";
	
	public static final String STATIC_URL = "staticUrl";

	public static final String REGISTRATION = "registration";
	public static final String SUPER_ADMIN_HOME = "superAdminHome";
	public static final String BIRDDOG_REGISTRATION = "birddogRegistration";
	public static final String SQUEEZE = "squeeze";
	public static final String BIRDDOG_HOME = "birddogHome";
	public static final String INVESTOR_HOME = "investorHome";
	public static final String ADD_LEAD = "addLead";
	public static final String COUNTRIES = "countries";
	public static final String REI_PROFILE = "reiProfile";
	public static final String INVESTOR_PROFILE = "updateReiProfile";
	public static final String SUPER_ADMIN_PROFILE = "updateSuperAdminProfile";
	public static final String BIRDDOG_PROFILE = "updateBirddogProfile";
	public static final String REI_LIST = "reiList";
	public static final String VIEW_REI = "viewRei";
	public static final String STATE = "state";
	public static final String CITY = "city";
	public static final String BIRDDOGS = "birddogs";
	public static final String SUCCESS_LEAD_LIST = "leadSuccessList";
	public static final String BIRDDOG_REPORTS = "birddogReports";
	public static final String BIRDDOGS_LIST = "birddogsList";
	public static final String PAYMENT_ALL_LEADS = "birddogPaymentAllLeads";
	public static final String VIEW_PAYMENTS = "viewTransactionHistory";
	public static final String SEND_PAYMENTS_FOR_ALL_BIRDDOG = "MassPay";
	public static final String ACCOUNT_SETTING = "accountSettings";
	public static final String PAYMENT_INTERVALS = "paymentIntervals";
	public static final String SERACH_BIRDDOG_LIST = "searchBirddogList";
	public static final String PAYMENT_BIRDDOG_LIST = "paymentBirddogList";
	public static final String PAYMENT_LEAD_LIST = "paymentLeadList";
	public static final String VIEW_BIRDDOG = "viewBirddog";
	public static final String MANAGE_BIRDDOG = "manageBirddog";
	public static final String LEADS = "leads";
	public static final String LEADS_LIST = "leadsList";
	public static final String REPORT_LEAD_LIST = "reportLeads";
	public static final String SEARCH_LEADS = "searchLeads";
	public static final Byte ZERO = 0;
	public static final String WELCOME_BIRDDOG_VIDEO = "welcomeBirddogVideo";
	public static final String WELCOME_INVESTOR_VIDEO = "welcomeInvestorVideo";
	public static final String ADD_USER_BY_REI = "addUserByRei";

	public static final String LEAD_SUCCESS = "CONGRATULATIONS! Your lead has been successfully entered for review for payment.Keep them coming!!";
	public static final String LEAD_UPDATED = "CONGRATULATIONS! Your lead has been  Updated successfully";
	
	public static final String OPEN = "OPEN";

	/* It is used for web service */
	public static final String RESPONSE = "response";
	public static final String DATA = "DATA";
	public static final String WS_MESSAGE = "MESSAGE";
	public static final String RESPONSE_SUCCESS_MESSAGE = "200";
	public static final String RESPONSE_NO_DATA_MESSAGE = "400";
	public static final String BIRDDOG_ID = "BirddogId";
	public static final String BIRDDOG_URL = "https://www.youtube.com/embed/";

	public static final String LEAD = "lead";
	public static final String IMAGES = "images";

	public static final String VIEW_LEAD = "viewLead";
	public static final String BIRDDOG_LEADS_LIST = "birddogLeadsList";
	public static final String VIEW_BIRDDOG_LEAD = "viewBirddogLead";
	public static final String VIEW_BIRDDOG_LIST = "groupMailToBirddog";
	public static final String COMMENTS = "comments";
	public static final String COMMENT = "comment";
	public static final String LEAD_ID = "leadId";
	public static final String SEND_MESSAGE = "sendMessage";
	public static final String BIRDDOG_MESSAGE = "birddogMessage";
	public static final String ADD_EMAIL = "addEmail";
	public static final String ADD_PAYPAL_EMAIL = "addPaypalEmail";
	public static final String SEARCH_REI_URL = "searchRei";
	public static final String ERROR_PAGE = "errors";
	public static final String PAGE_NOT_FOUND = "404";
	

	public static final String MESSAGE_LIST = "messageList";

	public static final String VIDEO_LIST = "video";
	public static final String VIEW_VIDEO = "viewVideo";

	public static final String VIEW_SUPER_ADMIN_TUTORIAL = "viewSuperAdminTutorial";
	public static final String VIDEO_FOR_REI = "viewVideoRei";
	public static final String VIDEO_FOR_BIRDDOG = "viewVideoBirddog";
	public static final String VIEW_SUPER_ADMIN_TUTORIAL_BIRDDOG = "viewTutorialBirddog";
	public static final String BIRDDOG_LEADS = "birddogLeads";
	public static final String SOURCE_QUALITY = "sourceQuality";

	public static final String WELCOME_BASIC_INVESTOR = "basicReiWelcome";
	public static final String WELCOME_PRO_INVESTOR = "proReiWelcome";
	public static final String WELCOME_MAX_INVESTOR = "maxReiWelcome";

	public static final String VIEW_TUTORIAL = "viewTutorial";
	public static final String VIEW_PDF = "viewPdf";
	public static final String VIEW_SOFTWARE = "viewSoftware";
	public static final String UPLOAD_TUTORIAL_REI = "uploadTutorialRei";
	public static final String UPLOAD_PDF = "uploadPdf";
	public static final String UPLOAD_SOFTWARE = "uploadSoftware";
	public static final String VIEW_SINGLE_TUTORIAL_REI = "viewSingleTutorialRei";
	public static final String VIEW_SINGLE_PDF_AND_SW = "viewSinglePdfAndSW";
	public static final String UPLOAD_TUTORIAL_BIRDDOG = "uploadTutorialBirddog";
	public static final String VIEW_SINGLE_TUTORIAL_BIRDDOG = "viewSingleTutorialBirddog";

	public static final String LEADS_SIZE = "leadsSize";

	public static final String PDF_FILE = "pdf";
	public static final String APP_FILE = "application";

	public static final String UPLOAD_TUTORIAL_BIRDDOG_BY_REI = "uploadTutorialBirddogByRei";
	public static final String VIEW_SINGLE_TUTORIAL_BIRDDOG_BY_REI = "viewSingleTutorialBirddogByRei";
	public static final String VIEW_REI_TUTORIAL_BY_SUPER_ADMIN = "viewReiTutorialBySuperAdmin";

	public static final String VIEW_BIRDDOG_TUTORIAL_BY_SUPER_ADMIN = "viewBirddogTutorialBySuperAdmin";
	public static final String VIEW_SINGLE_BIRDDOG_TUTORIAL_BY_ADMIN = "viewSingleBirddogTutorialByAdmin";

	public static final String VIEW_BIRDDOG_TUTORIAL_BY_INVESTOR = "viewBirddogTutorialByInvestor";
	public static final String VIEW_SINGLE_BIRDDOG_TUTORIAL_BY_INVESTOR = "viewSingleBirddogTutorialByInvestor";

	public static final String SUPER_ADMIN_CHANGE_PASSWORD = "superAdminChangePassword";
	public static final String FORGOT_PASSWORD = "forgotPassword";

	public static final String REI_CHANGE_PASSWORD = "reiChangePassword";
	public static final String BIRDDOG_CHANGE_PASSWORD = "birddogChangePassword";

	public static final String RECORD_NOT_FOUND = "records";
	public static final String RECORDS_NOT_FOUND = "message";
	public static final String REI_ACCOUNT_SETTING = "reiAccountSetting";

	public static final String PAY = "pay";
	public static final String MAIL_ID = "mailids";

	public static final String VIEW_PAYMENT_PAGE = "viewPayment";

	public static final String SHOW_MAP = "showMap";

	public static final String JPG = "jpg";

	public static final String FAILED = "(NULL)";
	public static final String EMPTY_FILE = "Unable to upload. File is empty.";

	public static final String TEXT_HTML = "text/html";
	public static final String VIEW_REI_PDF_AND_SW_BY_SUPER_ADMIN = "viewReiPdfAndSWBySuperAdmin";

	public static final String VIEW_BIRDDOG_PDF_AND_SW_BY_SUPER_ADMIN = "viewBirddogPdfAndSWBySuperAdmin";

	public static final String VIEW_SINGLE_BIRDDOG_ATTACHMENT = "viewSingleBirddogPdfAndSW";

	public static final String VIEW_SINGLE_REI_ATTACHMENT = "viewSingleReiPdfAndSW";

	public static final String THUMB_NIL_SQUEEZE_PAGE_URL = "thumbnailSqueezePage";

	public static final String AWS_ATTACHMENT_PATH = "https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=birddogattachments&prefix=Birddog_Profile_Images/";

	public static final String LEAD_IMAGE_PATH_WS = "https://s3-us-west-2.amazonaws.com/birddogattachments/Lead_Images/";
	public static final String VIDEO_PATH_WS = "https://s3-us-west-2.amazonaws.com/birddogattachments/Video/";
	public static final String BIRDDOG_PROFILE_IMAGE_PATH_WS = "https://s3-us-west-2.amazonaws.com/birddogattachments/Birddog_Profile_Images/";
	public static final String PDF_SOFTWARE_PATH_WS = "https://s3-us-west-2.amazonaws.com/birddogattachments/Pdf_Software/";
	public static final String PDF_ADMIN_SOFTWARE_PATH_WS = "https://s3-us-west-2.amazonaws.com/birddogattachments/pdf_Uploaded_By_Admin/";
	
	/*
	 * public static final String AWS_ATTACHMENT_PATH =
	 * "https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=birddogattachments&prefix=Birddog_Profile_Images/";
	 * 
	 * public static final String LEAD_IMAGE_PATH_WS =
	 * "https://s3-us-west-2.amazonaws.com/birddogattachments/Local/Lead_Images/";
	 * public static final String VIDEO_PATH_WS =
	 * "https://s3-us-west-2.amazonaws.com/birddogattachments/Local/Video/";
	 * public static final String BIRDDOG_PROFILE_IMAGE_PATH_WS =
	 * "https://s3-us-west-2.amazonaws.com/birddogattachments/Local/Birddog_Profile_Images/";
	 * public static final String PDF_SOFTWARE_PATH_WS =
	 * "https://s3-us-west-2.amazonaws.com/birddogattachments/Local/Pdf_Software/";
	 */

	public static final String REI_ACCOUNT_TYPE = "reiAccountType";
	public static final String REI = "rei";
	public static final String UPGRADE_REI_ACCOUNT_TYPE = "upgradeReiAccountType";
	public static final String MULTIPLE_MESSAGE_SEND = "multipleMessageSend";

	public static final String ALL_STATE = "states";
	public static final String ALL_CITY = "cities";
	public static final String ALL_COUNTIES = "counties";
	public static final String TYPE = "type";
	public static final String TUTORIAL_LIST = "tutorialList";
	public static final String UPLOAD_TUTORIAL = "UploadTutorial";
	public static final String TUTORIAL = "tutorial";

	public static final String SINGLE_SEND_MESSAGE = "singleSendMessage";
	public static final String VIEW_MESSAGE = "viewMessage";
	public static final String LIST = "list";
	public static final String DISPLAY = "display";
	public static final String USER_LIST = "userList";
	public static final String EMAIL_LIST = "emailList";
	public static final String PDF_LIST = "pdfList";
	public static final String FILE_TYPE = "fileType";
	public static final String ATTACHMENT = "attachment";
	public static final Integer COUNT = 5;
	public static final String ID = "id";
	public static final Byte NOTIFICATION = 1;
	public static final int APPROVED_STATUS = 1;
	public static final String UPLOAD_W9_FORM = "w9Form";
	public static final String BIRDDOG_W9_LIST = "birddogW9List";
	public static final String BIRDDOG_SAVE_PDF_LIST = "birddogsPdfList";

	public static final String ENCRYPTED_REI_ID = "encryptedReiId";

	public static final String BIRDDOG_PAY_ID = "birddogPayId";
	public static final String LEAD_PAY_ID = "leadPayId";
	public static final String VIEW_PAYMENT_PER_LEAD_PAGE = "viewPaymentPerLead";
	public static final String COMPLETE="complete";
	public static final String INCOMPLETE="incomplete";
	public static final String RESPONSE_PAGE_PAYMENT="Response";
	public static final String ERROR_PAGE_PAYMENT="Error";
	
	// seamless payment
	public static final String VIEW_SEAMLESS_PAGE= "paymentcheckout";
	public static final String REVIEW="review";
	public static final String RETURN = "return";
	public static final String CANCEL="cancel";
	
	public static final String MOBILE_LEAD_INSTRUCTION = "showMobileAppLeadInstruction";
	public static final String SHOW_BIRDDOG_NAVIGATION_GENERATION = "showBirddogNavigationGeneration";
	public static final String SHOW_REI_NAVIGATION_GENERATION = "showReiNavigationGeneration";
	public static final String CUSTOM_BD_TRANING = "/showCustomBirddogTraning";
	public static final String SHOW_LEAD_REPORT = "showLeadReport";
	
	public static final String REI_MANAGE_BIRDDOG = "/showManageBirddog";
	public static final String MESSAGE_BIRDDOG = "/showMessageBirddog";
	
	public static final String SHOW_PAY_BIRDDOG = "/showPayBirddog";
	public static final String SHOW_SETTING_SOFTWARE = "/showSettingSoftware";
	public static final String SHOW_W9_INSTRUCTION = "/showW9Instruction";
	public static final String SHOW_ADD_LEAD_VIDEO_INSTRUCTION = "/showAddLeadVideoInstruction";
	public static final String TRANING_INSTRUCTION = "/traningInstruction";
	public static final String REI_SUPPORT = "/showReisupport";
	public static final String SHOW_BIRDDOG_VIDEO_INSTRUCTION = "/showBirddogVideoInstructions";
	public static final String SHOW_REI_VIDEO_INSTRUCTION = "/showReiVideoInstructions";
	
	
}
