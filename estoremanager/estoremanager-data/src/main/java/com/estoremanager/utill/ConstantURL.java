


package com.estoremanager.utill;

import org.apache.tiles.Attribute;

public class ConstantURL {

	// URL Prefix
	// public static final String INVESTOR_URL =
	// "http://localhost:8089/birddog-web/birddogRegistration?REI=";
	// public static final String SQUEEZE_URL =
	// "http://localhost:8089/birddog-web/squeezeHome?REI=";

	public static final String INVESTOR_URL = "http://www.birddoggenerator.com/birddogRegistration?REI=";
	public static final String SQUEEZE_URL = "http://www.birddoggenerator.com/squeezeHome?REI=";

	// public static final String INVESTOR_URL =
	// "http://103.9.15.59:8081/birddog-web/birddogRegistration?REI=";
	public static final Attribute BASE_TEMPLATE = new Attribute("/WEB-INF/layout/defaultLayout.jsp");

	// System Path
	public static final String RESOURCE_HANDLER = "/resources/**";
	public static final String RESOURCE_LOCATION = "/resources/";

	// layout component
	public static final String JSP_PACKAGE = "/WEB-INF/jsp/";
	public static final String HEADER = "/WEB-INF/layout/header.jsp";
	public static final String FOOTER = "/WEB-INF/layout/footer.jsp";
	public static final String CUSTOM_FOOTER = "/WEB-INF/layout/customFooter.jsp";
	public static final String ADMIN_HEADER = "/WEB-INF/layout/adminHeader.jsp";
	public static final String REI_HEADER = "/WEB-INF/layout/reiHeader.jsp";
	public static final String SQUEEZE_DUMP = "/WEB-INF/layout/squeezeDump.jsp";
	public static final String BIRDDOG_HEADER = "/WEB-INF/layout/birddogHeader.jsp";
	public static final String JSP_EXTANTION = ".jsp";

	// Action URL
	public static final String REGISTRATION_URL = "/registration";
	public static final String HOME_URL = "/home";
	public static final String SAVE_REGISTRATION_URL = "/saveRegistration";
	public static final String BIRDDOG_REGISTRATION_URL = "/birddogRegistration";
	public static final String SQUEEZE_PAGE_URL = "/squeezePage";
	public static final String SQUEEZE_HOME_PAGE_URL = "/squeezeHome";
	public static final String SAVE_TRANSACTION = "/saveTransactions";
	public static final String VIEW_TRANSACTION_DETAILS = "/viewTransactionDetails";
	public static final String SAVE_BIRDDOG_REGISTRATION_URL = "/saveBirddogRegistration";
	public static final String LOGIN_URL = "/login";
	public static final String WELCOME_URL_PATTERN = "/welcome/**";
	public static final String LOGIN_FAILIOUR = "/login?error";
	public static final String WELCOME_URL = "/welcome";
	public static final String BIRDDOG_HOME_URL = "/birddogHome";
	public static final String SAVE_REI_PROFILE = "/saveReiProfile";
	public static final String SAVE_ACCOUNT_SETTING_URL = "/saveAccountSettings";
	public static final String SEND_MESSAGE_TO_BIRDDOG = "/sendMessageToMultipleBirddog";
	public static final String SEND_MULTIPLE_MESSAGE = "/sendMultipleMessage";
	public static final String REI_PROFILE_URL = "reiProfile";
	public static final String LOGOUT_URL = "/login?logout";
	public static final String ERROR_URL = "/error";
	public static final String ADD_LEAD_URL = "/addLead";
	public static final String SAVE_LEAD_URL = "/saveLead";
	public static final String UPDATE_LEAD_URL = "/updateLead";
	public static final String NOTIFY_URL = "/notifyPage";
	public static final String STATE_BY_COUNTRY_ID_URL = "/stateByCountryId";
	public static final String CITY_BY_STATE_ID_URL = "/cityByStateId";
	public static final String CANCEL_URL = "/cancel";
	public static final String VIEW_REI_LIST_URL = "/viewReiList";
	public static final String SEARCH_REI_LIST_URL = "/searchReiList";
	
	public static final String API_INFO = "/apiInfo";
	
     	
	public static final String SEARCH_REI_BY_NAME_OR_DATE = "/searchReiByNameAndDate";
	public static final String VIEW_REI_URL = "/viewRei";
	public static final String GROUP_MAIL_TO_BIRDDOG_URL = "/groupMailToBirddog";
	public static final String SEND_MAIL_TO_BIRDDOG_URL = "/sendMailToBirddog";
	public static final String GROUP_MAIL_TO_BIRDDOG = "groupMailToBirddog";
	public static final String DELETE_BIRDDOG_URL = "/deleteBirddog";
	public static final String DELETE_BIRDDOG_MANAGE_URL = "/deleteBirddogManage";
	public static final String DELETE_LEAD_URL = "/deleteLead";
	public static final String PERMANENT_DELETE_LEAD_URL = "/permanentDeleteLead";
	public static final String CHANGE_ARCHIVE_LEAD_STATUS = "/changeArchiveLeadStatus";
	public static final String REI_SIDE_CHANGE_ARCHIVE_LEAD_STATUS = "/changeArchiveLeadStatusOnReiSide";
	public static final String SEARCH_BY_DATE_URL = "/searchByDate";
	public static final String INVESTOR_HOME_URL = "/InvestorHome";
	public static final String VIEW_REI_PAYMENT_INFO = "/viewReiPaymentInfo";
	
	
	
	
	public static final String BASIC_REI_HEADER = "/WEB-INF/layout/basicReiHeader.jsp";
	public static final String PRO_REI_HEADER = "/WEB-INF/layout/proReiHeader.jsp";
	public static final String MAX_REI_HEADER = "/WEB-INF/layout/maxReiHeader.jsp";

	// multipartResolver

	public static final String BIRDDOG_PROFILE_URL = "/birddogProfile";
	public static final String BIRDDOG_PROFILE_URL_FOR_HOME_PAGE = "/birddogProfileForHome";
	public static final String SAVE_BIRDDOG_PROFILE_URL = "/saveBirddogProfile";
	public static final String VIEW_BIRDDOGS_URL = "/viewBirddogs";
	public static final String MANAGE_BIRDDOGS_URL = "/manageBirddogs";
	public static final String VIEW_ALL_BIRDDOG = "/viewAllBirddogs";
	public static final String PAYMENT_ALL_LEADS_URL = "/paymentAllLeads";
	public static final String DELETE_ATTACHMENT_BY_ID_URL = "/deleteAattachmentByAttachmentId";
	public static final String VIEW_BIRDDOG_URL = "/viewBirddog";
	public static final String SAVE_REI_EMAIL_URL = "/saveReiMailId";
	public static final String GET_CITY_URL = "/getCity";
	public static final String GET_COUNTY_URL = "/getCounty";
	public static final String VIEW_LEADS_URL = "viewLeads";
	public static final String ARCHIVE_LEADS_ON_VIEW_LEADS_REI_SIDE = "archiveLeadsOnViewLeadsReiSide";
	public static final String VIEW_ALL_LEADS_FOR_REPORT_PAGE = "viewAllLeads";
	public static final String ARCHIVE_LEADS_REI_SIDE = "viewArchiveLeadsOnReiSide";
	public static final String VIEW_LEADS_FOR_REPORT_URL = "viewLeadsForReports";
	public static final String REPORT_LEADS_URL = "reportLeads";
	public static final String VIEW_LEAD_URL = "/viewLead";
	public static final String VIEW_PAYMENT = "/viewPayment";
	public static final String VIEW_NOTIFY_LEAD_URL = "/viewNotifyLead";
	public static final String VIEW_STATUS_LEAD_URL = "/viewStatusLead";
	public static final String SEARCH_BIRDDOG_BY_REI_URL = "/searchBirddogByRei";
	public static final String VIEW_PAYMENT_BY_LEAD_URL = "/viewPaymentByLead";
	public static final String SEARCH_LEAD_BY_AJAX = "/searchLeadByAjax";
	public static final String VIEW_ARCHIVE_LEADS = "/viewArchiveLeads";
	public static final String SEARCH_LEAD_BY_REI_URL = "/searchLeadByRei";
	public static final String SEARCH_REPORT_LEAD_BY_REI_URL = "/searchReportLeadByRei";
	public static final String SEARCH_LEAD_BY_BIRDDOG_DATE_URL = "/searchLeadByBirddogIdAndDate";
	public static final String DELETE_INVESTOR_URL = "/deleteInvestor";
	public static final String SEARCH_DELETE_INVESTOR_URL = "/searchDeleteInvestor";
	public static final String STATE_URL = "/getAllState";
	public static final String GET_COUNTY_BY_STATE_URL = "/getCountyByState";
	public static final String GET_CITY_BY_COUNTY_URL = "/getCityByCounty";
	public static final String UPDATE_BIRDDOG_LEAD_URL = "/updateBirddogLead";
	public static final String ADD_COMMENT_URL = "/addComment";
	public static final String SHOW_ACCOUNT_SETTING_URL = "/showAccountSetting";
	public static final String SEND_PAYMENT_URL = "/sendPayment";
	public static final String LEAD_STATUS_LIST = "/leadStatusList";
	public static final String LEAD_STATUS = "/leadStatus";
	public static final String LEAD_APPROVAL = "/leadApproval";
    public static final String LEAD_APPROVAL_LIST = "/leadApprovalStatus";
    public static final String BLOCK_BIRDDOG_MANAGE_URL = "/blockBirddogManage";
	// webservice Action URL
	public static final String BIRDDOG_LOGIN_URL = "/getBirddogLoginDetail";
	public static final String LEAD_URL = "/getAllLead";
	public static final String SAVE_WS_LEAD_URL = "/saveLeadDetails";
	public static final String CITY_BY_COUNTY_ID_URL = "/cityByCountyId";
	public static final String COUNTY_BY_STATE_ID_URL = "/countyByStateId";
	public static final String VIEW_BIRDDOG_LEADS_URL = "/viewBirddogLeads";
	public static final String VIEW_BIRDDOG_LEAD_URL = "/viewBirddogLead";
	public static final String BIRDDOG_PROFILE_WS_URL = "/viewBirddogProfile";
	public static final String UPDATE_BIRDDOG_PROFILE_WS_URL = "/updateBirddogProfile";
	public static final String BIRDDOG_WS_CHANGE_PASSWORD = "/ChangePassword";
	public static final String BIRDDOG_FORGOT_PASSWORD_WS = "/birddogForgotPasswordWs";
	public static final String WS_LOGIN_URL = "/getBirddogLoginDetail";
	public static final String BIRDDOG_ADDCOMMENT_WS_URL = "/birddogAddCommentWS";
	public static final String SYNC_WS_URL = "/syncWS";
	public static final String GET_ALL_COMMENT_WS_URL = "/getAllCommentWs";
	public static final String GET_MESSAGE_WS_URL = "/getAllMessage";
	public static final String VIEW_ALL_BIRDDOG_TUTORIAL_WS = "/viewAllBirddogTutorialWs";
	public static final String VIEW_SINGLE_LEAD_WS = "/viewSingleLeadWs";
	public static final String VIEW_SUPER_ADMIN_TUTORIAL_FOR_BIRDDOG_WS = "/viewSuperAdminTutorialForBirddogWs";
	public static final String VIEW_SINGLE_SUPER_ADMIN_TUTORIAL_FOR_BIRDDOG_WS = "/viewSingleSuperAdminTutorialForBirddogWs";
	public static final String NOTIFICTION_LEAD_URL = "/notificationLead";
	public static final String NOTIFICTION_APPROVED_STATUS_URL = "/notificationApprovedStatus";
	public static final String NOTIFICTION_BIRDDOG_URL = "/notificationBirddog";
	public static final String SEND_MESSAGE_URL = "/sendMessage";
	public static final String SINGLE_SEND_MESSAGE_URL = "/singleSendMessage";
	public static final String MESSAGE_URL = "/message";
	public static final String SAVE_MESSAGE_URL = "/saveMessage";
	public static final String MESSAGES_URL = "/messages";
	public static final String SAVE_ADD_USER_URL = "/saveAddUser";
	public static final String SAVE_ADD_EMAIL_URL = "/saveAddEmail";
	public static final String SAVE_PAYPAL_EMAIL_URL = "/savePaypalEmail";
	public static final String UPDATE_EMAIL_URL = "/editEmail";
	public static final String UPDATE_PAYPAL_EMAIL_URL = "/editPaypalEmail";
	public static final String SEARCH_REI_CREATION_DATE_BY_ADMIN_URL = "/searchReiCreationDateByAdmin";
	public static final String SUPER_ADMIN_CHANGE_PASSWORD_URL = "/superAdminChangePassword";
	public static final String SHOW_ADMIN_CHANGE_PASSWORD_PAGE_URL = "/showAdminChangePassword";
	public static final String SHOW_FORGOT_PASSWORD_PAGE_URL = "/showForgotPasswordPage";
	public static final String FORGOT_PASSWORD = "/forgotPassword";
	public static final String SHOW_REI_CHANGE_PASSWORD_PAGE_URL = "/showReiChangePasswordPage";
	public static final String REI_CHANGE_PASSWORD_URL = "/reiChangePassword";
	public static final String SHOW_BIRDDOG_CHANGE_PASSWORD_PAGE_URL = "/showBirddogChangePassword";
	public static final String BIRDDOG_CHANGE_PASSWORD_URL = "/birddogChangePassword";
	public static final String UPDATE_LEAD_DETAIL_WS = "/updateLeadWS";
	public static final String BIRDDOG_GET_COMMENT_WS_URL = "/getALLCommentWS";
	public static final String VERIFY_USER_EMAILID = "/verifyUserEmailId";
	public static final String DELETE_LEAD_DETAIL_WS = "/deleteLeadDetailWS";
	public static final String SEARCH_LEAD_DATE_WISE_WS = "/serchLeadDateWise";
	public static final String WELCOME_BIRDDOG_VIDEO_URL = "/welcomeBirddogVideo";
	public static final String GET_ALL_BIRDDOGS_LIST_URL = "/getAllBirddogsList";
	public static final String LIST_OF_EMAIL = "/listOfEmail";
	public static final String BIRDDOG_REPORT_URL = "/birddogUrl";
	public static final String ADD_USER_URL = "/addUser";
	public static final String ADD_EMAIL_URL = "/addEmail";
	
	public static final String ADD_BIRDDOG_PAYPAL_UNIQUE_WEB_URL = "/addBirddogPaypalWebUrl";
   	public static final String VIEW_Birddog_PAYPAL_URL = "/viewBirddogPaypalUrl";
   	public static final String VIEW_PAYMENT_BY_REI_URL = "/viewPaymentByRei";

	public static final String ADD_PAYPAL_EMAIL_URL = "/birddogPaypal";

	public static final String SHOW_MAP_URL = "/showMapPage";
	public static final String SENDPAYMENTS = "/sendBirddogsPayment";
	public static final String UPLOAD_TUTORIAL_BIRDDOG_BY_REI_URL = "/uploadTutorialBirddogByRei";
	public static final String UPLOAD_SUPER_ADMIN_VIEDO_URL = "/uploadSuperAdminVideo";
	public static final String VIEW_VIDEO_URL = "/viewVideo";
	public static final String TUTORIALS_URL = "/tutorial";
	public static final String SEARCH_REI_URL = "/searchRei";
	public static final String PDF_URL = "/pdf";
	public static final String SOFTWARE_URL = "/software";
	public static final String REI_TUTORIAL_URL = "/reiTutorial";
	public static final String VIEW_SUPER_ADMIN_TUTORIAL_URL = "/viewSuperAdminTutorial";
	public static final String VIDEO_FOR_REI_URL = "/viewVideoRei";
	public static final String VIDEO_FOR_BIRDDOG_URL = "/viewVideoBirddog";
	public static final String VIEW_SUPER_ADMIN_TUTORIAL_BIRDDOG_URL = "/viewTutorialBirddog";
	public static final String SAVEVIDEO_URL = "/savevideo";
	public static final String SAVE_TUTORIAL_BIRDDOG_BY_REI_URL = "/saveTutorialBirddogByRei";
	public static final String UPDATE_TUTORIAL_BIRDDOG_BY_REI_URL = "/updateTutorialBirddogByRei";
	public static final String DELETE_TUTORIAL_BIRDDOG_BY_REI_URL = "/deleteTutorialBirddogByRei";
	public static final String VIEW_SINGLE_TUTORIAL_BIRDDOG_BY_REI_URL = "/viewSingleTutorialBirddogByRei";
	public static final String VIEW_REI_TUTORIAL_BY_SUPER_ADMIN_URL = "/viewReiTutorialBySuperAdmin";
	public static final String VIEW_BIRDDOG_TUTORIAL_BY_SUPER_ADMIN_URL = "/viewBirddogTutorialBySuperAdmin";
	public static final String VIEW_SINGLE_BIRDDOG_TUTORIAL_BY_ADMIN_URL = "/viewSingleBirddogTutorialByAdmin";
	public static final String VIEW_BIRDDOG_TUTORIAL_BY_INVESTOR_URL = "/viewBirddogTutorialByInvestor";
	public static final String VIEW_SINGLE_BIRDDOG_TUTORIAL_BY_INVESTOR_URL = "/viewSingleBirddogTutorialByInvestor";
	public static final String UPLOAD_TUTORIAL_REI_URL = "/uploadTutorialRei";

	public static final String UPLOAD_PDF = "/uploadPdf";
	public static final String UPLOAD_SOFTWARE = "/uploadSoftware";

	public static final String SAVE_TUTORIAL_REI_URL = "/saveTutorialRei";
	public static final String SAVE_PDF = "/savePdf";
	public static final String SAVE_SOFTWARE = "/saveSoftware";
	public static final String UPDATE_SUPER_ADMIN_TUTORIAL_REI_URL = "/updateSuperAdminTutorialRei";
	public static final String UPDATE_SUPER_ADMIN_PDF_AND_SW_URL = "/updateSuperAdminPdfAndSW";
	public static final String DELETE_SUPER_ADMIN_TUTORIAL_REI_URL = "/deleteSuperAdminTutorialRei";
	public static final String DELETE_SUPER_ADMIN_PDF_AND_SW_URL = "/deleteSuperAdminPdfAndSW";
	public static final String VIEW_SINGLE_TUTORIAL_REI_URL = "/viewSingleTutorialRei";
	/*public static final String VIEW_SINGLE_PDF_AND_SW_URL = "/viewSinglePdfAndSW";*/
	public static final String UPLOAD_TUTORIAL_BIRDDOG_URL = "/uploadTutorialBirddog";
	public static final String SAVE_TUTORIAL_BIRDDOG_URL = "/saveTutorialBirddog";
	public static final String UPDATE_SUPER_ADMIN_TUTORIAL_BIRDDOG_URL = "/updateSuperAdminTutorialBirddog";
	public static final String DELETE_SUPER_ADMIN_TUTORIAL_BIRDDOG_URL = "/deleteSuperAdminTutorialBirddog";
	public static final String VIEW_SINGLE_TUTORIAL_BIRDDOG_URL = "/viewSingleTutorialBirddog";
	public static final String SEARCH_BY_BIRDDOG_ON_MAP_URL = "/searchByBirddogOnMap";
	public static final String SEARCH_BY_CITY_ON_MAP_URL = "/searchByCityOnMap";

	public static final String VIEW_SINGLE_ATTACHMENT_URL = "/viewSinglePdfAndSW";

	public static final String VIEW_BIRDDOG_PDF_AND_SW_BY_SUPER_ADMIN_URL = "/viewBirddogPdfAndSWBySuperAdmin";

	public static final String SAVE_PDF_AND_SW = "/savePdf";
	public static final String VIEW_ALL_SW_AND_PDF_WS = "/getAllPdfAndSW";
	public static final String VIEW_SINGLE_SW_AND_PDF_WS = "/getSinglePdfAndSW";

	public static final String VIEW_REI_ACCOUNT_TYPE = "/reiAccountType";
	public static final String UPGRADE_REI_ACCOUNT_TYPE = "/upgradeReiAccountType";
	public static final String SAVE_REI_ACCOUNT_TYPE = "/saveRiAccountType";

	public static final String DELETE_EMAIL_URL = "/deleteEmailId";
	public static final String DELETE_PAYPAL_EMAIL_URL = "/deletePaypalEmail";

	public static final String UPDATE_LEAD_STATUS = "updateLeadStatus";
	public static final String UPDATE_LEAD_APPROVAL = "updateLeadApproval";
	public static final String GET_ALL_LEAD_LIST_BY_BIRDDOG = "/getAllLeadListByBirddog";

	public static final String DISPLAY_BIRDDOG_LIST = "/displayBirddogList";
	public static final String SEARCH_BIRDDOG_BY_ZIPCODE = "/searchBirddogByZipCode";
	public static final String VIEW_NOTIFY_BIRDDOG_URL = "/viewNotifyBirddogs";
	
	public static final String GET_LEADS_BY_DATE = "getLeadsByDate";
	public static final String GET_LEADS = "getLeads";
	
	public static final String SEARCH_BIRDDOG_BY_CITY = "/searchBirddogByCity";
	public static final String SEARCH_BIRDDOG_BY_COUNTY = "/searchBirddogByCounty";
	public static final String SEARCH_BIRDDOG_BY_MONTH = "/searchByMonth";
	public static final String PDF_FORM = "/pdfform";
	public static final String UPLOAD_W9PDF = "/uploadW9Pdf";
	public static final String VIEW_BIRDDOG_W9_FORM = "/viewBirddogW9formList";

	public static final String THUMB_NIL_SQUEEZE_PAGE_URL = "/thumbnailSqueezePage";
	public static final String NOTIFY_PER_LEAD_URL = "/notifyPerLeadPage";

	public static final String SEND_PAYMENT_PER_LEAD_URL = "/sendPaymentPerLead";
	
	public static final String VIEW_IMAGE_STATUS = "/viewImageStatus";
	public static final String MANAGE_BIRDDOG_REPORT_URL = "/managerReportBirddogs";
	public static final String VIEW_ALL_LEADS_FOR_BIRDDOG = "/viewAllLeadsForBirddog";
	public static final String SEARCH_BIRDDOG_BY_CITY_COUNTY_STATE = "/searchBirddogByCityCountyState";
	public static final String SEARCH_LEAD_BY_CITY_COUNTY_STATE = "/searchLeadByCityStateCounty";
	public static final String USER_LOGIN = "/userLogin";
	public static final String UPLOAD_W9_FROM_URL = "/uploadW9form";
	
	// seamless payment
	public static final String VIEW_CHECKOUT_PAGE="/viewpaymentbir";
	public static final String SEND_PAYMENT_BIRDDOG="/SendPaymentBIrddog";
	public static final String RETURN_SEAMLESS_URL = "/Return";
	public static final String VIEW_TRANSACTION = "/viewTransaction";
	
	public static final String MOBILE_APP_LEAD_INSTRUCTION = "/showMobileAppLeadInstruction";
	public static final String SHOW_BIRDDOG_NAVIGATION_GENERATION = "/showBirddogNavigationGeneration";
	public static final String SHOW_REI_NAVIGATION_GENERATION = "/showReiNavigationGeneration";
	public static final String CUSTOM_BD_TRANING = "/showCustomBirddogTraning";
	public static final String SHOW_LEAD_REPORT = "/ShowLeadReport";
	
	public static final String SHOW_MANAGE_BIRDDOG = "/showManageBirddog";
	public static final String MESSAGE_BIRDDOG = "/showMessageBirddog";
	
	public static final String SHOW_PAY_BIRDDOG = "/showPayBirddog";
	public static final String SHOW_SETTING_SOFTWARE = "/showSettingSoftware";
	public static final String SHOW_W9_INSTRUCTION = "/showW9Instruction";
	public static final String Add_NEW_LEAD_INSTRUCTION = "/addNewLeadDesktopInstruction";
	public static final String TRANING_INSTRUCTION = "/traningInstruction";
	public static final String REI_SUPPORT = "/reiSupport";
	public static final String SHOW_BIRDDOG_VIDEO_INSTRUCTION = "/showBirddogVideoInstructions";
	public static final String SHOW_REI_VIDEO_INSTRUCTION = "/showReiVideoInstructions";
	public static final String SAVE_INFUSTION_DATA = "/get-data.asp";
	
	
	
}
